<?php
/*
 * MIT License

    Copyright (c) 2020 Benoit MOTTIN

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.

 */

session_start();

require_once "./src/function/Database.php";

require_once "src/models/Film.php";
require_once "src/controller/FilmController.php";

$filmController = new FilmController();

$films = $filmController->getFilms();


include 'header.inc.php';
?>


<div class="container">
    <div class="card-deck">
        <?php
        foreach ($films as $film) {
            echo '
                    <div class="card">
                        <div class="card-header">' . $film->getTitle() . '</div>
                        <div class="card-body">
                            <img src="' . $film->getJaquette() . '" width="200px">
                            <p>' . $film->getExcerp(200) . '</p>
                        </div>
                        <div class="card-footer">
                            <a href="show_film.php?id=' . $film->getId() . '">
                                <button class="btn btn-primary">Voir +</button>
                            </a>
                        </div>
                    </div>';
        }
        ?>



    </div>
</div>

<?php

include 'footer.inc.php';
